# file_chooser.py
#
# Copyright 2023 Miniature Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from os import path

from gi.repository import Adw, GLib, Gtk

from .supported_image_types import SUPPORTED_IMAGE_TYPES


class FileChooser:
    """Open and load file."""

    @staticmethod
    def open_file(parent, last_view_page, *args):
        def __on_response(_dialog, response):
            """Run if the user selects a file."""
            if response == Gtk.ResponseType.ACCEPT:
                parent.check_is_image(_dialog.get_file())
            else:
                parent.main_stack.set_visible_child_name(last_view_page)

        dialog = Gtk.FileChooserNative.new(
            title=_("Select a file"), parent=parent, action=Gtk.FileChooserAction.OPEN
        )

        dialog.set_modal(True)
        dialog.connect("response", __on_response)

        file_filter = Gtk.FileFilter.new()
        file_filter.set_name(_("Supported image files"))
        for img_type in SUPPORTED_IMAGE_TYPES:
            file_filter.add_mime_type("image/" + img_type)
        dialog.add_filter(file_filter)

        dialog.show()

    @staticmethod
    def save_file(parent, img_instance, default_name):
        def __on_save_file(file):
            save_path = file.get_path()

            print(f"Output file: {save_path}")

            # Translators: Do not translate "{save_path}"
            toast = Adw.Toast.new(_('"{save_path}" saved').format(save_path=save_path))

            extension = path.splitext(file.get_basename())[1]
            filetype = extension[1:].lower()
            if filetype == "":
                toast.set_title(_("No file extension specified"))
            if filetype not in SUPPORTED_IMAGE_TYPES:
                toast.set_title(
                    # Translators: Do not translate "{filetype}"
                    _('"{filetype}" is an unsupported image format').format(
                        filetype=filetype
                    )
                )
            else:
                img_instance.save(save_path)

                toast.set_button_label(_("Open"))
                toast.set_action_name("app.open-output")
                toast.set_action_target_value(GLib.Variant("s", save_path))

            parent.toast_overlay.add_toast(toast)

        def __on_response(_dialog, response):
            """Run if the user selects a file."""
            if response == Gtk.ResponseType.ACCEPT:
                __on_save_file(_dialog.get_file())

        dialog = Gtk.FileChooserNative.new(
            title=_("Select a file"), parent=parent, action=Gtk.FileChooserAction.SAVE
        )

        dialog.set_modal(True)
        dialog.connect("response", __on_response)
        dialog.set_current_name(default_name)
        dialog.show()
