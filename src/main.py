# main.py
#
# Copyright 2023 Miniature Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

from gi.repository import Adw, Gio, GLib

from .pasting import Paster
from .window import MiniatureWindow


class MiniatureApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(
            application_id="io.gitlab.gregorni.Miniature",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
        )
        self.__create_action(
            "quit", lambda *_: self.quit(), ["<primary>q", "<primary>w"]
        )
        self.__create_action(
            "open-menu",
            lambda *_: self.get_active_window().menu_btn.activate(),
            ["F10"],
        )
        self.__create_action(
            "open-file",
            lambda *_: self.get_active_window().on_open_file(),
            ["<primary>o"],
        )
        self.__create_action("paste-image", self.__paste_image, ["<primary>v"])
        self.__create_action("about", self.__on_about_action)
        self.__create_action(
            "open-output", self.__open_output, param=GLib.VariantType("s")
        )
        self.file = None

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.get_active_window()
        if not win:
            win = MiniatureWindow(application=self)
        win.present()
        if self.file is not None:
            win.check_is_image(Gio.File.new_for_path(self.file))

    def __paste_image(self, *args):
        win = self.get_active_window()
        Paster().paste_image(win, win.check_is_image)

    def __open_output(self, app, data):
        file = open(data.unpack(), "r")
        Gio.DBusProxy.new_sync(
            connection=Gio.bus_get_sync(Gio.BusType.SESSION, None),
            flags=Gio.DBusProxyFlags.NONE,
            info=None,
            name="org.freedesktop.portal.Desktop",
            object_path="/org/freedesktop/portal/desktop",
            interface_name="org.freedesktop.portal.OpenURI",
            cancellable=None,
        ).call_with_unix_fd_list_sync(
            method_name="OpenFile",
            parameters=GLib.Variant(
                "(sha{sv})", ("", 0, {"ask": GLib.Variant("b", True)})
            ),
            flags=Gio.DBusCallFlags.NONE,
            timeout_msec=-1,
            fd_list=Gio.UnixFDList.new_from_array([file.fileno()]),
            cancellable=None,
        )

    def __on_about_action(self, *args):
        """Callback for the app.about action."""
        about = Adw.AboutWindow.new_from_appdata(
            "/io/gitlab/gregorni/Miniature/appdata.xml"
        )
        about.set_transient_for(self.get_active_window())
        # about.set_artists()
        about.set_developer_name(_("Miniature Contributors"))
        # These are Python lists: Add your string to the list (separated by a comma)
        # See the translator comment below for possible formats
        about.set_developers(["gregorni https://gitlab.gnome.org/gregorni"])
        about.set_copyright(_("Copyright © 2023 Miniature Contributors"))
        # Translators: Translate this string as your translator credits.
        # Name only:    gregorni
        # Name + URL:   gregorni https://gitlab.gnome.org/gregorni/
        # Name + Email: gregorni <gregorniehl@web.de>
        # Do not remove existing names.
        # Names are separated with newlines.
        about.set_translator_credits(_("translator-credits"))

        about.add_acknowledgement_section(
            _("Code and Design borrowed from"),
            [
                "Upscaler https://gitlab.gnome.org/World/Upscaler",
                "Footage https://gitlab.com/adhami3310/Footage",
            ],
        )

        about.present()

    def do_command_line(self, command_line):
        args = command_line.get_arguments()
        if len(args) > 1:
            self.file = command_line.create_file_for_arg(args[1]).get_path()
        self.activate()
        return 0

    def __create_action(self, name, callback, shortcuts=None, param=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
            param: an optional list of parameters for the action
        """
        action = Gio.SimpleAction.new(name, param)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    return MiniatureApplication().run(sys.argv)
