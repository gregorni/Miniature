# window.py
#
# Copyright 2023 Miniature Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from os import path

from gi.repository import Adw, Gdk, Gio, GLib, Gtk
from PIL import Image, ImageChops, ImageOps

from . import texture_to_file
from .file_chooser import FileChooser
from .supported_image_types import SUPPORTED_IMAGE_TYPES


@Gtk.Template(resource_path="/io/gitlab/gregorni/Miniature/gtk/window.ui")
class MiniatureWindow(Adw.ApplicationWindow):
    __gtype_name__ = "MiniatureWindow"

    menu_btn = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    drag_revealer = Gtk.Template.Child()
    main_stack = Gtk.Template.Child()
    spinner = Gtk.Template.Child()
    img_display_pic = Gtk.Template.Child()
    orig_size_row = Gtk.Template.Child()
    percent_spin = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        target = Gtk.DropTarget(
            formats=Gdk.ContentFormats.new_for_gtype(Gio.File),
            actions=Gdk.DragAction.COPY,
        )

        target.set_gtypes([Gdk.Texture, Gio.File])
        target.connect("drop", self.__on_drop)
        target.connect("enter", self.__on_enter)
        target.connect("leave", self.__on_leave)
        self.add_controller(target)

        settings = Gio.Settings(schema_id="io.gitlab.gregorni.Miniature")
        settings.bind(
            "window-width", self, "default-width", Gio.SettingsBindFlags.DEFAULT
        )
        settings.bind(
            "window-height", self, "default-height", Gio.SettingsBindFlags.DEFAULT
        )
        settings.bind(
            "window-is-maximized", self, "maximized", Gio.SettingsBindFlags.DEFAULT
        )

        self.save_btn.connect("clicked", self.__save_to_file)

        self.previous_stack = "welcome"
        self.file = None

    def on_open_file(self):
        self.__show_spinner()
        FileChooser.open_file(self, self.previous_stack)

    def check_is_image(self, file):
        def __wrong_image_type():
            print(f"{file.get_path()} is not of a supported image type.")
            self.toast_overlay.add_toast(
                Adw.Toast.new(
                    # Translators: Do not translate "{basename}"
                    _('"{basename}" is not of a supported image type.').format(
                        basename=file.get_basename()
                    )
                )
            )
            self.main_stack.set_visible_child_name(self.previous_stack)

        try:
            input_file_path = file.get_path()
            with Image.open(input_file_path) as img:
                if self.file and file:
                    with Image.open(self.file.get_path()) as old_img:
                        same_image = not ImageChops.difference(
                            old_img.convert("RGB"), img.convert("RGB")
                        ).getbbox()
                        if same_image:
                            self.main_stack.set_visible_child_name(self.previous_stack)
                            return

                self.__show_spinner()
                print(f"Input file: {input_file_path}")

                if img.format.lower() in SUPPORTED_IMAGE_TYPES:
                    self.file = file
                    img = ImageOps.exif_transpose(img)
                    img = img.convert("RGBA")
                    self.__display_image(img)
                else:
                    __wrong_image_type()
        except IOError:
            __wrong_image_type()

    def __display_image(self, img):
        width, height = img.size

        self.img = img
        self.main_stack.set_visible_child_name("display-page")
        self.orig_size_row.set_subtitle(f"{width} x {height}")
        self.percent_spin.set_value(100)

        texture = Gdk.MemoryTexture.new(
            width,
            height,
            Gdk.MemoryFormat.R8G8B8A8,
            GLib.Bytes.new(img.tobytes()),
            # "Stride" is the amount of bytes per row in a given image.
            # The R8G8B8A8 format is 4 bytes long (each channel is a byte (8-bits)), so we multiply the width by 4 to obtain the stride.
            width * 4,
        )
        self.img_display_pic.set_paintable(texture)

    def __save_to_file(self, *args):
        split_input_filename = path.splitext(path.basename(self.file.get_path()))
        file_extension = split_input_filename[1]
        output_path = f"{split_input_filename[0]}-downscaled{file_extension}"
        FileChooser.save_file(self, self.img, output_path)

    def __show_spinner(self):
        self.main_stack.set_visible_child_name("spinner-page")
        self.spinner.start()

    def __on_drop(self, widget, drop, *args):
        failed_as_file = False
        try:
            self.check_is_image(drop)
        except:
            failed_as_file = True

        if failed_as_file:
            try:
                file = texture_to_file.to_file(drop)
                self.check_is_image(file)
            except:
                toast = Adw.Toast.new(_("Dropped item is not a valid image"))
                self.toast_overlay.add_toast(toast)

    def __on_enter(self, *args):
        self.drag_revealer.set_reveal_child(True)
        return Gdk.DragAction.COPY

    def __on_leave(self, *args):
        self.drag_revealer.set_reveal_child(False)
